# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170527194721) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "documents", force: :cascade do |t|
    t.string   "doc_type"
    t.integer  "no_versions"
    t.string   "pe_num"
    t.string   "committee"
    t.string   "reg_url"
    t.string   "procedure"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "iter_extract_id"
    t.string   "doc_lang"
    t.string   "title"
    t.date     "iter_creation"
    t.date     "translation_submission"
    t.integer  "page_count"
    t.boolean  "reg_ok",                 default: false, null: false
    t.boolean  "oeil_ok",                default: false, null: false
    t.string   "oeil_url"
    t.string   "procedure_type"
    t.string   "template_name"
    t.string   "ax_ref"
    t.boolean  "pe_num_not_in_iter",     default: false, null: false
    t.integer  "ams_count"
  end

  add_index "documents", ["iter_extract_id"], name: "index_documents_on_iter_extract_id", using: :btree

  create_table "iter_extracts", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "role"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "documents", "iter_extracts"
end
