class AddRegOkToDocument < ActiveRecord::Migration
  def change
    add_column :documents, :reg_ok, :boolean, null: false, default: false
  end
end