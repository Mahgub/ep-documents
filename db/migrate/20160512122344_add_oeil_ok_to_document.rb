class AddOeilOkToDocument < ActiveRecord::Migration
  def change
    add_column :documents, :oeil_ok, :boolean, null: false, default: false
  end
end
