class AddPeNumNotInIterToDocument < ActiveRecord::Migration
  def change
    add_column :documents, :pe_num_not_in_iter, :boolean, null: false, default: false
  end
end
