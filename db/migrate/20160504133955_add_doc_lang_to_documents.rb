class AddDocLangToDocuments < ActiveRecord::Migration
  def change
    add_column :documents, :doc_lang, :string
  end
end
