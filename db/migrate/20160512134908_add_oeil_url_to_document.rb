class AddOeilUrlToDocument < ActiveRecord::Migration
  def change
    add_column :documents, :oeil_url, :string
  end
end
