class AddProcedureTypeToDocument < ActiveRecord::Migration
  def change
    add_column :documents, :procedure_type, :string
  end
end
