class AddIterCreationToDocument < ActiveRecord::Migration
  def change
    add_column :documents, :iter_creation, :date
  end
end
