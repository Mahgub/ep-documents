class AddIterExtarctRefToDocuments < ActiveRecord::Migration
  def change
    add_reference :documents, :iter_extract, index: true, foreign_key: true
  end
end