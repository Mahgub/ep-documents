class RenameUrlToRegUrlInDocument < ActiveRecord::Migration
  def change
    rename_column :documents, :url, :reg_url
  end
end
