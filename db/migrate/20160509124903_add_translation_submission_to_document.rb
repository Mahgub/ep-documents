class AddTranslationSubmissionToDocument < ActiveRecord::Migration
  def change
    add_column :documents, :translation_submission, :date
  end
end
