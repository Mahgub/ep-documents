class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :type
      t.integer :no_versions
      t.string :pe_num
      t.string :committee
      t.string :url
      t.string :procedure

      t.timestamps null: false
    end
  end
end
