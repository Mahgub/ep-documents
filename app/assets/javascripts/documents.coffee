# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'page:change', ->
  $('.ui.dropdown').dropdown()

  $('.menu .item').tab()

  # https://github.com/ankane/chartkick/issues/28
  # trigger window resize event upon tab switch
  $('.menu .item').click (e) ->
    ev = document.createEvent('Event')
    ev.initEvent 'resize', true, true
    window.dispatchEvent ev

  $('.selectable').popup
    inline: false
    hoverable: true
    position: 'top left'
    delay:
      show: 300

  # Compatibility of rails form submission with Semantic UI loading class Butotn
  $("#filter_submit_button").click (e) ->
    $(".filter.form").submit()

  return
return




