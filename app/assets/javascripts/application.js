// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require turbolinks

// Loads all Semantic javascripts
//= require semantic-ui/api
//= require semantic-ui/colorize
//= require semantic-ui/embed
//= require semantic-ui/form
//= require semantic-ui/state
//= require semantic-ui/visibility
//= require semantic-ui/visit
//= require semantic-ui/site
//= require semantic-ui/accordion
//= require semantic-ui/checkbox
//= require semantic-ui/dimmer
//= require semantic-ui/dropdown
//= require semantic-ui/modal
//= require semantic-ui/nag
//= require semantic-ui/popup
//= require semantic-ui/progress
//= require semantic-ui/rating
//= require semantic-ui/search
//= require semantic-ui/shape
//= require semantic-ui/sidebar
//= require semantic-ui/sticky
//= require semantic-ui/tab
//= require semantic-ui/transition
//= require semantic-ui/video

//= require_tree .


$(document).ready(function() {
    Turbolinks.enableProgressBar();
    $('.button.loading').on('load', stop_button_loader($(this)));

// Loaders and Spinners
    var spinning_icon = $('<i class="ui spinner loading icon"></i>');
    function stop_button_loader() {
        $(this).removeClass('loading')
    }
    function start_button_loader() {
        $(this).addClass('loading')
    }

    // Spinner for any Semantic Button
    $('.ui.button').on('click', start_button_loader);

    // Closing of modal, needs a refresh
    $('.actions .ui.close.button').on('click', function () {
        stop_button_loader();
        $('.ui.upload.modal').modal('hide');
    });

// Other UI Elements
    $('.ui.checkbox').checkbox();

    $('.message .close').on('click', function () {
            $(this)
                .closest('.message')
                .transition('fade');
        });

    // Main Menu Popup
    $('.menu .iter').popup({
            popup      : '.extract.popup',
            inline     : true,
            hoverable  : true,
            position   : 'bottom left',
            transition : 'fade',
            preserve   : true,
            delay: {
                show: 100,
                hide: 400
            },
            onHide: function () {
                $('.button.loading').removeClass('loading');
            }
        });

    $('.upload.button').on('click', function () {
        $('.extract.popup').popup('hide');
        $('.ui.upload.modal').modal({
                                duration    : 200,
                                transition  : 'scale'
                                })
                            .modal('show');
    });

    // Spinner Main Menu - if there are Menu Icons
    $('.menu .container .item').on('click', function() {
        $(this).children('i').first().replaceWith(spinning_icon);
    });

    $('.menu .item.single_extract_line').on('click', function () {
        $(this).children('.right.floated.content')
            .append(spinning_icon);
    });
});

// Clean up after page:restore event, which translates to History Back
$(window).on('page:restore', function() {
    $('.button.loading').removeClass('loading');
    $('.right.floated.content i.loading').hide();
});

// TO-DO: Remove spinners from Main Menu when going back