class FetchDocumentDataJob < ActiveJob::Base
  queue_as :default

  def perform(document_id)
    document = Document.find(document_id)
    document.update_urls
    document.update_reg_and_oeil_booleans
    if document.oeil_url or document.reg_url
      document.download_and_store
      document.parse_file
      document.update_procedure_no
      document.update_procedure_type
      document.update_template_name
      document.update_page_count
      document.update_ams_count
      #document.delete_downloaded_file
    end
  end
end