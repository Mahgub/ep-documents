class IterExtract < ActiveRecord::Base
  has_many :documents, dependent: :destroy
  accepts_nested_attributes_for :documents

  validates_associated :documents

  def self.import(file)
    new_import = IterExtract.create!
    CSV.foreach(file.path, headers: true) do |row|
      row = row.to_hash
      # {"Reference"=>"PE573.128" or "A8-0057/2015"
      doc_reference = row["Reference"]
      pe_ref = false
      ax_ref = false
      if /PE\d{3}.\d{3}/.match doc_reference
        pe_ref = doc_reference
      elsif /A\d-\d{4}\/\d{4}/.match doc_reference
        ax_ref = doc_reference
      end
      # "Contribution"=>nil,
      contribution = row["Contribution"]
      # "Version"=>"v01-00",
      version = row["Version"][2].to_i
      # "Title"=>"European Commission: Consolidated annual accounts of the European Union - Financial Year 2014 - European Environment Agency (EEA)",
      title = row["Title"]
      # "Family"=>"AME - Amendment",
      family = row["Family"]
      # "Type"=>"AMCO (AM) - Amendments in committee to draft report or opinion",
      doc_type = row["Type"][6..7]
      # "Original language"=>"en",
      language = row["Original language"].upcase
      # "Status"=>"Ended",
      status = row["Status"].downcase
      # "Originating institution"=>"PE - European Parliament",
      source_institution = row["Originating institution"]
      # "Organ"=>"ENVI - Environment, Public Health and Food Safety",
      committee = row["Organ"][0..3]
      # "FDR Number"=>"1081326",
      fdr = row["FDR Number"]
      # "FDR Status"=>"Workflow finished",
      fdr_status = row["FDR Status"]
      # "Document date"=>"22/12/2015",
      translation_submission_date = row["Document date"].to_date if row["Document date"].size == 10

      # "Registered"=>"09/12/2015",
      iter_creation_date = row["Registered"].to_date if row["Registered"].size == 10
      # "KEY_FDRNUMBER"=>"1081326",
      rules = row["Rules"]
      # "Rules"=>nil}

      unless status == "in preparation" || family == "DEP - Tabled text"#don't import files with "in preparation"
        #binding.pry
        new_import.documents.where(pe_num: pe_ref, doc_type: doc_type).first_or_initialize do |doc|
          if doc.no_versions && doc.no_versions < version
            doc.no_versions = version
          else
            doc.no_versions = version
          end
          doc.doc_lang = language
          doc.committee = committee
          doc.title = title
          doc.iter_creation = iter_creation_date
          doc.translation_submission = translation_submission_date
          doc.save
          FetchDocumentDataJob.perform_later doc.id
        end
      end
    end
  end

end