require 'open-uri'
class Document < ActiveRecord::Base
  belongs_to :IterExtract
  validates :pe_num, uniqueness: {scope: :doc_type}, allow_blank: true

  scope :by_version,   -> (ver)       { where(no_versions: ver) }
  scope :by_committee, -> (committee) { where(committee: committee) }
  scope :by_procedure, -> (procedure) { where(procedure_type: procedure) }
  scope :by_doc_type,  -> (doc_type)  { where(doc_type: doc_type) }
  scope :by_year,      -> (year)      { where('extract(year from translation_submission) = ?', year) }

  def update_page_count
    return false unless download_and_store
    count = Docsplit.extract_length(local_file)
    self.page_count = count
    self.save
  end

  def pseudo_filename(extension = ".pdf")
    extension = ".pdf" unless extension == ".doc" || extension == ".txt"
    base = "#{self.pe_num}" + "-v0" + "#{self.no_versions}" + ".00"
    base + extension if extension
  end

  def local_file(path=Rails.public_path.join('local_copies'))
    supported_extensions = [".pdf", ".doc"]
    supported_extensions.each do |ext|
      if File.exist?(path.join(pseudo_filename(ext)))
        return path.join(pseudo_filename(ext))
      end
    end
    false
  end

  def parsed_file?(path=Rails.public_path.join('parsed_texts'), ext=".txt")
    return false unless File.exist?(path.join(pseudo_filename(ext)))
    true
  end

  def parsed_filename(path=Rails.public_path.join('parsed_texts'), ext=".txt")
    return false unless File.exist?(path.join(pseudo_filename(ext)))
    path.join(pseudo_filename(ext))
  end

  def update_urls
    self.reg_url = fetch_register_url if !!reg_url or !!fetch_register_url
    self.oeil_url = fetch_oeil_url if !!oeil_url or !!fetch_oeil_url
    self.save
  end

  def update_reg_and_oeil_booleans
    self.toggle!(:reg_ok) if !!fetch_register_url && !self.reg_ok
    self.toggle!(:oeil_ok) if !!fetch_oeil_url && !self.oeil_ok
  end

  def generate_reg_ref
   self.pe_num[2..8]
  end

  def fetch_register_url(bypass = false)
    return reg_url if !!self.reg_url unless bypass
    base = "http://www.europarl.europa.eu/RegistreWeb/search/simple.htm?relValue="
    doc = Nokogiri::HTML(open(base+generate_reg_ref))
    return false if doc.css('div.warning_message').length > 0
    url = doc.css('ul.documents li a').first["href"]
    return false if self.return_http_code(url) != "200"
    url
  end

  def fetch_oeil_url(bypass = false)
    return oeil_url if !!self.oeil_url unless bypass
    ref = pe_num[0..1]+"-"+pe_num[2..8]
    language = "EN" #because publication :/
    sec_ref = "0#{self.no_versions}"
    version = "0#{no_versions}"

    # url = 'http://www.europarl.europa.eu/sides/getDoc.do?type=COMPARL&reference='+ref+'&format=PDF&language='+language+'&secondRef='+sec_ref

    url = "http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//NONSGML+COMPARL+#{ref}+#{version}+DOC+PDF+V0//EN&language=EN"

    # http://www.europarl.europa.eu/sides/getDoc.do?pubRef=PE-594.019+01+DOC+PDF+V0//EN&language=EN
    # PA -> http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//NONSGML+COMPARL+PE-544.351+01+DOC+PDF+V0//EN&language=EN
    # PR -> http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//NONSGML+COMPARL+PE-594.019+01+DOC+PDF+V0//EN&language=EN
    # AM -> http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//NONSGML+COMPARL+PE-589.492+02+DOC+PDF+V0//EN&language=EN
    return false if self.return_http_code(url) != "200"
    url
  end

  def download_and_store(path=Rails.public_path.join('local_copies'))
    return true if !!local_file
    if !!oeil_url
      url = oeil_url
    elsif !!reg_url
      url = reg_url
    else
      return false
    end
    open(path.join(pseudo_filename(url.last(4))), 'wb') do |file|
      file << open(url).read
    end
  end

  def delete_downloaded_file
    File.delete(local_file)
  end

  def parse_file
    if local_file && !parsed_file?
      Docsplit.extract_text local_file,
                            ocr: false,
                            output: Rails.public_path.join('parsed_texts')
    end
  end

  # TODO - DRY the "update methods"
  def update_procedure_no
    self.procedure = fetch_procedure_no
    self.save
  end

  def fetch_procedure_no
    return false unless parsed_file?
    file = File.open(parsed_filename, 'r')
    file.each do |line|
      return /\d{4}\/\d{4}\([A-Z]{3}\)/.match(line).to_s if /\d{4}\/\d{4}\([A-Z]{3}\)/.match line
      return "B-type" if /AM_Com_NonLegRE/.match line
    end
    "ERROR"
  end

  def update_procedure_type
    self.procedure_type = fetch_procedure_type
    self.save
  end

  def fetch_procedure_type
    if /\d{4}\/\d{4}\([A-Z]{3}\)/.match procedure
      (/\([A-Z]{3}\)/.match(procedure)).to_s
    else
      procedure
    end
  end

  def update_ams_count
    if fetch_amendment_no
      self.ams_count = fetch_amendment_no
      self.save
    end
  end

  def fetch_amendment_no
    return false unless parsed_file?

    amendmentMatcher = /^AMENDMENT.*$/
    amendmentNumMatcher = /^\d*\s*-\s*\d*$/
    amCounterIdx = -1
    value = nil

    text = File.open(parsed_filename, 'r').read
    text.each_line.with_index do |line, index|

      if amendmentMatcher.match(line)
        amCounterIdx = index+1
      end

      if index==amCounterIdx && amendmentNumMatcher.match(line)
        val = line.strip.split('-')
          if val.length == 1
            value = val.first.to_i if val.length == 1
          else
            value = val.last.to_i - val.first.to_i + 1
          end
      end
    end
    value
  end

  def update_template_name
    self.template_name = fetch_template_name
    self.save
  end

  def fetch_template_name
    return false unless parsed_file?
    file = File.open(parsed_filename, 'r')
    file.each do |line|
      #debugger
      return /AM_Com_\w+/.match(line).to_s if /AM_Com_\w+/.match(line)
      return /PA_\w+/.match(line).to_s if /PA_\w+/.match(line)
      return /PR_\w+/.match(line).to_s if /PR_\w+/.match(line)
      return /P\d_TA-PROV/.match(line).to_s if /P\d_TA-PROV/.match(line)
      return /P\d_TA/.match(line).to_s if /P\d_TA/.match(line)
      return /ERRATUM/.match(line).to_s if /ERRATUM/.match(line)
    end
    "ERROR"
  end

  def return_http_code(url)
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    response.code
  end
end
