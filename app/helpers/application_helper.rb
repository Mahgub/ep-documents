module ApplicationHelper
  def giveFullPageTitle(page_title = "")
    base_title = I18n.t 'application'
    if page_title.empty?
      base_title
    else
      page_title + " - " + base_title
    end
  end
end
