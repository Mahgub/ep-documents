module DocumentsHelper
  # horizontal sum = DOC TYPES / COMMITTEES TYPES
  # division = month / year / quarter
  # value = Doc Type
  def document_per_month(documents)
    #
    # Data Output
    # [
    # {
    #     name: "EMPL",
    #     data: [["jan 15", 10], ["feb 15", 16], ["march 15", 28]]
    # },
    #     {
    #         name: "ENVI",
    #         data: [["jan 15", 24], ["feb 15", 22], ["march 15", 19]]
    #     },
    #     {
    #         name: "CONT",
    #         data: [["jan 15", 20], ["feb 15", 23], ["march 15", 29]]
    #     }
    # ]
    #

    result = []
    return result if documents.empty?
    start_date = documents.group(:translation_submission).count.sort.first[0]
    end_date = documents.group(:translation_submission).count.sort.last[0]
    #(start_date..end_date)

    duration = (start_date..end_date).map { |d| Date.new(d.year, d.month, 1)}.uniq
    ordered_month_list = Hash[duration.zip(Array.new(duration.length,0))]

    temp = documents.group(:committee).count.sort { |l, r| l[1]<=>r[1] }
    ordered_committee_list = []
    # Ger Ordered Committee List
    temp.reverse.each { |entry| ordered_committee_list << entry[0] }

    # Start Committee Loop
    ordered_committee_list.each do |committee|

      committee_data = Hash.new

      temp_data = documents.where(committee: committee).group(:translation_submission).count
      # {Tue, 20 Jan 2015=>1, Tue, 27 Jan 2015=>1, Wed, 28 Jan 2015=>1, Thu, 05 Mar 2015=>22, Fri, 06 Mar 2015=>27, Mon, 09 Mar 2015=>3, Tue, 24 Mar 2015=>1, Mon, 30 Mar 2015=>1, Thu, 11 Jun 2015=>1, Fri, 04 Sep 2015=>6, Fri, 25 Sep 2015=>1, Wed, 11 Nov 2015=>1, Fri, 04 Dec 2015=>1, Mon, 07 Dec 2015=>1}
      data = {}
      data = ordered_month_list.dup

      temp_data.each do |date|
        period = date[0].beginning_of_month
        value = date[1]
        data[period] = 0 unless data[period]
        data[period] += value
      end
      #data = {Thu, 01 Jan 2015=>3, Sun, 01 Mar 2015=>54, Mon, 01 Jun 2015=>1, Tue, 01 Sep 2015=>7, Sun, 01 Nov 2015=>1, Tue, 01 Dec 2015=>2}

      committee_data[:name] = committee
      data = data.to_a
      data.each_with_index do |entry, index|
        data[index][0] = entry[0].to_formatted_s(:short_month_and_year)
      end
      committee_data[:data] = data
      result << committee_data
    end
    #binding.pry
    result
  end

  def document_per_years(documents)
    result = []
    return result if documents.empty?
    start_date = documents.group(:translation_submission).count.sort.first[0]
    end_date = documents.group(:translation_submission).count.sort.last[0]
    duration = (start_date..end_date).map { |d| Date.new(d.year) }.uniq
    ordered_years_list = Hash[duration.zip(Array.new(duration.length,0))]
    temp = documents.group(:committee).count.sort { |l, r| l[1]<=>r[1] }
    ordered_committee_list = []
    # Ger Ordered Committee List
    temp.reverse.each { |entry| ordered_committee_list << entry[0] }

    # Start Committee Loop
    ordered_committee_list.each do |committee|

      committee_data = Hash.new

      data = documents.where(committee: committee).group_by_year(:translation_submission).count

      #data = {Thu, 01 Jan 2015=>3, Sun, 01 Mar 2015=>54, Mon, 01 Jun 2015=>1, Tue, 01 Sep 2015=>7, Sun, 01 Nov 2015=>1, Tue, 01 Dec 2015=>2}

      committee_data[:name] = committee
      data = data.to_a
      data.each_with_index do |entry, index|
        data[index][0] = entry[0].year
      end
      committee_data[:data] = data
      result << committee_data
    end
    result
  end

  def amendments_per_years(documents)
    result = []
    return result if documents.empty?
    start_date = documents.group(:translation_submission).count.sort.first[0]
    end_date = documents.group(:translation_submission).count.sort.last[0]
    duration = (start_date..end_date).map { |d| Date.new(d.year) }.uniq
    ordered_years_list = Hash[duration.zip(Array.new(duration.length,0))]
    temp = documents.group(:committee).sum(:ams_count).sort { |l, r| l[1]<=>r[1] }
    ordered_committee_list = []
    # Ger Ordered Committee List
    temp.reverse.each { |entry| ordered_committee_list << entry[0] }

    # b = a.sort_by { |committee| committee[:data].last }.reverse



    # Start Committee Loop
    ordered_committee_list.each do |committee|

      committee_data = Hash.new

      data = documents.where(committee: committee).group_by_year(:translation_submission).sum(:ams_count)
      #data = {Thu, 01 Jan 2015=>3, Sun, 01 Mar 2015=>54, Mon, 01 Jun 2015=>1, Tue, 01 Sep 2015=>7, Sun, 01 Nov 2015=>1, Tue, 01 Dec 2015=>2}

      committee_data[:name] = committee
      data = data.to_a
      data.each_with_index do |entry, index|
        data[index][0] = entry[0].year
      end
      committee_data[:data] = data
      result << committee_data
    end
    result
  end

  def amendments_per_month(documents)
    #
    # Data Output
    # [
    # {
    #     name: "EMPL",
    #     data: [["jan 15", 10], ["feb 15", 16], ["march 15", 28]]
    # },
    #     {
    #         name: "ENVI",
    #         data: [["jan 15", 24], ["feb 15", 22], ["march 15", 19]]
    #     },
    #     {
    #         name: "CONT",
    #         data: [["jan 15", 20], ["feb 15", 23], ["march 15", 29]]
    #     }
    # ]
    #

    result = []
    return result if documents.empty?
    start_date = documents.group(:translation_submission).count.sort.first[0]
    end_date = documents.group(:translation_submission).count.sort.last[0]
    #(start_date..end_date)

    duration = (start_date..end_date).map { |d| Date.new(d.year, d.month, 1)}.uniq
    ordered_month_list = Hash[duration.zip(Array.new(duration.length,0))]

    temp = documents.group(:committee).sum(:ams_count).sort { |l, r| l[1]<=>r[1] }
    ordered_committee_list = []
    # Ger Ordered Committee List
    temp.reverse.each { |entry| ordered_committee_list << entry[0] }

    # Start Committee Loop
    ordered_committee_list.each do |committee|

      committee_data = Hash.new

      data = documents.where(committee: committee).group_by_month(:translation_submission).sum(:ams_count)
      #data = {Thu, 01 Jan 2015=>3, Sun, 01 Mar 2015=>54, Mon, 01 Jun 2015=>1, Tue, 01 Sep 2015=>7, Sun, 01 Nov 2015=>1, Tue, 01 Dec 2015=>2}

      committee_data[:name] = committee
      data = data.to_a
      data.each_with_index do |entry, index|
        data[index][0] = entry[0].to_formatted_s(:short_month_and_year)
      end
      committee_data[:data] = data
      result << committee_data
    end
    #binding.pry
    result
  end


  def number_of_pages(documents)
    result = []
    return result if documents.empty?
    values = documents.group(:page_count).count.except(nil).to_a
    values = Hash[values.map { |key, val| [ key.to_i, val.to_i ]} ]
    values = Hash[values.sort_by { |key, val| key } ]
    values.each { |value| result << value }
    result # returns Array of Arrays
  end

  def submission_time_offset(documents)
    result = [['Time Offset', 'Incidence']]
    # result = []
    return result if documents.empty?
    a = documents.pluck(:translation_submission)
    b = documents.pluck(:iter_creation)
    offset_array = a.map.with_index { |date, index| (date - b[index]).to_int}
    values = offset_array.inject(Hash.new(0)) { |h, e| h[e] += 1 ; h }.to_a
    values.each { |value| result << value.to_i }
  end

  def height_of_chart(documents)
    if documents.group_by_month(:translation_submission).count.size > 12
      "800px"
    else
      "400px"
    end
  end

  def versions_array(documents)
    documents.group(:no_versions).count
  end

  def doc_types_array(documents)
    temp = documents.group(:doc_type).count
    temp.map { |entry| entry.first }
  end

  def committees_array(documents)
    temp = documents.group(:committee).count.sort { |l, r| l.second <=> r.second }.reverse
    temp.map { |entry| entry.first }
  end

  def procedures_array(documents)
    temp = documents.group(:procedure_type).count.sort { |l, r| l[1]<=>r[1] }.reverse
    temp.map { |entry| entry.first }
  end

  def years_array(documents)
    documents.group(:translation_submission).count.map { |e| e.first.year }.uniq.sort
  end

  def translation_year(document)
    document.translation_submission.year
  end

  def creation_time_drift(document)
    (document.translation_submission - document.iter_creation).to_i
  end

  def time_drift_chart(documents)
    result = []
    return result if documents.empty?
    time_drift = documents.per_page(documents.count).map { |doc| creation_time_drift(doc) }
    time_drift_hash = Hash.new 0 # so that is default value 0 for all new entries
    time_drift.each do |val|
      time_drift_hash[val] += 1
    end
    time_drift_hash.sort
  end
end