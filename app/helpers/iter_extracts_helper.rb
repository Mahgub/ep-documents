module IterExtractsHelper
  def time_range(iter_extract_id)
    single_extract = IterExtract.find(iter_extract_id)
    unless single_extract.documents.empty?
      months = single_extract.documents.group_by_month(:translation_submission).count.map { |k,v| k }
      start_date = months.first.to_formatted_s(:short_month_and_year)
      end_date = months.last.to_formatted_s(:short_month_and_year)
      return "#{start_date} - #{end_date}"
    end
    "No documents"
  end

  def error_count(iterextract)
    err_procedure = iterextract.documents.where(procedure: "ERROR").count
    err_template = iterextract.documents.where(template_name: "ERROR").count
    err_ulrs = iterextract.documents.where(oeil_ok: false).where(reg_ok: false).count
    err_procedure + err_template + err_ulrs
  end
end