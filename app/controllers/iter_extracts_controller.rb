class IterExtractsController < ApplicationController

  def index
    @iter_extracts = IterExtract.all.order(:created_at)
  end

  def import
    IterExtract.import(params[:file])
    redirect_to iter_extracts_url, notice: 'ITER Extract Successfully Imported.
                                           Documents are now being processed - it will take a few minutes.'
  end

  def destroy

  end
end