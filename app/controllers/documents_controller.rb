class DocumentsController < ApplicationController
  include DocumentsHelper
  # Scopes with keyword arguments need to be called in a block
  has_scope :by_committee, type: :array
  has_scope :by_version, type: :array
  has_scope :by_procedure, type: :array
  has_scope :by_doc_type, type: :array
  has_scope :by_year, type: :array

  def index
    extract_id ||= params[:iter_extract_id]
    if extract_id
      @documents = apply_scopes(Document).where(iter_extract_id: extract_id)
                         .paginate(per_page: 100, page: params[:page])
      available_docs = Document.where(iter_extract_id: extract_id)
    else
      @documents = apply_scopes(Document).paginate(per_page: 100, page: params[:page])
      available_docs = Document.all
    end

    @available_versions = versions_array(available_docs)
    @available_committees = committees_array(available_docs)
    @available_procedures = procedures_array(available_docs)
    @available_doc_types = doc_types_array(available_docs)
    @available_years = years_array(available_docs)

    @selected_versions = (params[:by_version].present? ? params[:by_version] : [])
    @selected_committees = (params[:by_committee].present? ? params[:by_committee] : [])
    @selected_procedures = (params[:by_procedure].present? ? params[:by_procedure] : [])
    @selected_doc_types = (params[:by_doc_type].present? ? params[:by_doc_type] : [])
    @selected_years = (params[:by_year].present? ? params[:by_year] : [])
  end

  def show
  end
end
