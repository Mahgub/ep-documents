Date::DATE_FORMATS[:month_and_year] = '%B %Y'
Date::DATE_FORMATS[:short_month_and_year] = '%b %Y'
Date::DATE_FORMATS[:short_month_and_short_year] = '%b %Y'
Date::DATE_FORMATS[:short_ordinal] = ->(date) { date.strftime("%B #{date.day.ordinalize}") }
Date::DATE_FORMATS[:short_month] = ->(date) { date.strftime("%b")}