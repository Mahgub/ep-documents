require 'sidekiq/web'

Rails.application.routes.draw do
  get 'static_pages/home'

  get 'static_pages/help'

  devise_for :users, path_names: {sign_in: 'login', sign_out: 'logout'}

  get 'documents/index'

  get 'documents/show'

  resources :iter_extracts do
    collection { post :import }
    resources :documents, only: [:index, :show]
  end

  root to: "static_pages#home"

  authenticate :user do
    mount Sidekiq::Web, at: "/sidekiq"
  end
end